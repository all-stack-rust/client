#![recursion_limit="256"]

use wasm_bindgen::prelude::*;
use yew::prelude::*;
use plotters::prelude::*;
use yew::web_sys;
use yew::services::{WebSocketService};
use yew::services::websocket::{WebSocketTask, WebSocketStatus};
use std::fmt::{Display, Formatter};
use communication::Tx;
use fft_calculator::FftData;

static CAPTION: &str = "All stack rust fft example";

struct Model {
    link: ComponentLink<Self>,
    plot: String,
    websocket_task: Option<WebSocketTask>,
}

struct FormatWsStatus {
    status: WebSocketStatus,
}

impl Display for FormatWsStatus {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let ws_status = match self.status {
            WebSocketStatus::Closed => {
                "Closed"
            }
            WebSocketStatus::Error => {
                "Error"
            }
            WebSocketStatus::Opened => {
                "Opened"
            }
        };
        write!(f, "{}", ws_status)
    }
}

enum Msg {
    Go,
    IncomingFft(Vec<u8>),
    WsStatusChange(FormatWsStatus),
    CloseWebsocket
}

impl Model {

    pub fn draw_empty_plot(&mut self) {
        self.plot.clear();
        let root = SVGBackend::with_string(&mut self.plot, (640, 480)).into_drawing_area();
        root.fill(&WHITE).unwrap();

        let mut chart = ChartBuilder::on(&root)
            .caption(CAPTION, ("sans-serif", 32).into_font())
            .margin(5)
            .x_label_area_size(50)
            .y_label_area_size(50)
            .build_ranged(-1.0f32..1.0f32, -1.0f32..1.0f32).unwrap();

        chart
            .configure_mesh()
            .axis_desc_style(("sans-serif", 18))
            .x_desc("Frequency / [Hz]")
            .y_desc("Magnitude")
            .draw()
            .unwrap();

        chart
            .configure_series_labels()
            .background_style(&WHITE.mix(0.8))
            .border_style(&BLACK)
            .draw().unwrap();
    }

    pub fn draw_plot(&mut self, fft_data: FftData) {
        self.plot.clear();
        let root = SVGBackend::with_string(&mut self.plot, (640, 480)).into_drawing_area();
        root.fill(&WHITE).unwrap();

        let mut x_max = fft_data.x_data[fft_data.x_data.len() - 1];
        let x_min = 0.0f32 - (x_max * 0.1f32);
        x_max = x_max * 1.1f32;

        let mut y_max = fft_data.fft_data.clone().into_iter().max_by(|a, b| {
            return a.norm().partial_cmp(&b.norm()).unwrap()
        }).unwrap().norm();
        y_max = y_max + (y_max * 0.1f32);
        let mut y_min = fft_data.fft_data.clone().into_iter().min_by(|a, b| {
            return a.norm().partial_cmp(&b.norm()).unwrap()
        }).unwrap().norm();
        y_min = y_min - (y_min * 0.1f32);

        let mut chart = ChartBuilder::on(&root)
            .caption(CAPTION, ("sans-serif", 32).into_font())
            .margin(5)
            .x_label_area_size(50)
            .y_label_area_size(100)
            .build_ranged(x_min..x_max, y_min..y_max).unwrap();

        chart
            .configure_mesh()
            .axis_desc_style(("sans-serif", 18))
            .x_desc("Frequency / [Hz]")
            .y_desc("Magnitude")
            .draw()
            .unwrap();

        let fft_iter = fft_data.fft_data.into_iter()
            .zip(fft_data.x_data)
            .map(| (mag, f)| {
                (f, mag.norm())
            });

        chart
            .draw_series(LineSeries::new(fft_iter,
                &RED,
            )).unwrap()
            .label("ADC audio sampling")
            .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &RED));

        chart
            .configure_series_labels()
            .background_style(&WHITE.mix(0.8))
            .border_style(&BLACK)
            .draw().unwrap();
    }
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();
    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let mut me = Self {
            link,
            plot: "<svg></svg>".to_string(),
            websocket_task: None,
        };
        me.draw_empty_plot();
        return me;
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Go => {
                if self.websocket_task.is_some() {
                    yew::web_sys::console::log_1(
                        &JsValue::from("Websocket already open")
                    );
                    return false;
                }

                yew::web_sys::console::log_1(
                    &JsValue::from("Create websocket")
                );

                let window = yew::web_sys::window().unwrap();
                let location = window.location();

                let mut target = "ws://".to_string();
                target.push_str(location.hostname().unwrap().as_str());
                target.push_str(":");
                target.push_str(location.port().unwrap().as_str());

                yew::web_sys::console::log_1(
                    &JsValue::from(format!("Attempting to connect to: {}", target))
                );

                let close_ws_callback: Callback<()> = self.link.callback( |_| Msg::CloseWebsocket);

                let ws_task = WebSocketService::connect_binary::<Result<Vec<u8>, anyhow::Error>>(
                    target.as_str(),
                    self.link.callback(|bin: Result<Vec<u8>, anyhow::Error> | {
                        Msg::IncomingFft(bin.unwrap())
                    }),
                    self.link.callback( move |status| {
                        if status == WebSocketStatus::Closed || status == WebSocketStatus::Error {
                            close_ws_callback.emit(());
                        }

                        Msg::WsStatusChange(FormatWsStatus {
                            status,
                        })
                    })
                );

                match &ws_task {
                    Ok(_ws) => {
                        yew::web_sys::console::log_1(
                            &JsValue::from("Connected websocket")
                        );
                    }
                    Err(err) => {
                        yew::web_sys::console::log_1(
                            &JsValue::from(format!("Failed to create socket: {}", *err))
                        );
                    }
                }

                self.websocket_task = ws_task.ok();
            },
            Msg::IncomingFft(bin) => {
                let tx = Tx::deserialize(&bin[..]).unwrap();
                let mut fft_data = fft_calculator::FftData::from_slice(&tx.samples[..], tx.sample_time);
                fft_data.fft_shift();
                self.draw_plot(fft_data);
            },
            Msg::WsStatusChange(status) => {
                yew::web_sys::console::log_1(
                    &JsValue::from(format!("Ws status: {}", status).as_str())
                );
            },
            Msg::CloseWebsocket => {
                yew::web_sys::console::log_1(
                    &JsValue::from("Destroyed websocket")
                );
                self.websocket_task = None;
            }
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        html! {
            <div class="container">
                <div></div>
                <div class="element">
                    <div class="fft-container">
                        <div id="plot-container"></div>
                        <div class="button-container">
                            <button type="button" class="btn btn-primary" onclick=self.link.callback(|_| Msg::Go)>{ "Go" }</button>
                            <button type="button" class="btn btn-danger" onclick=self.link.callback(|_| Msg::CloseWebsocket)>{ "Stop" }</button>
                        </div>
                    </div>
                </div>
                <div></div>
            </div>
        }
    }

    fn rendered(&mut self, _first_render: bool) {
        let window = web_sys::window().expect("window not available");
        let document = window.document().expect("document not available");
        let div = document.get_element_by_id("plot-container").expect("Div not found");
        div.set_inner_html(&self.plot);
    }
}

#[wasm_bindgen(start)]
pub fn run_app() {
    App::<Model>::new().mount_to_body();
}
