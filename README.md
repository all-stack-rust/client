# Build 
 wasm-pack build --debug --target web --out-name wasm --out-dir ./static

# Test locally 
 miniserve ./static --index index.html

# Optimize
## Optimize for size.
wasm-opt -Os -o output.wasm input.wasm
## Optimize aggressively for size.
wasm-opt -Oz -o output.wasm input.wasm
## Optimize for speed.
wasm-opt -O -o output.wasm input.wasm
## Optimize aggressively for speed.
wasm-opt -O3 -o output.wasm input.wasm
